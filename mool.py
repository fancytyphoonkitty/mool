#!/usr/bin/env python3
import requests
import argparse
import sys
import json
import re
import coloredlogs
import logging
import os
from netaddr import EUI, mac_cisco
from netaddr.core import AddrFormatError

def setup_custom_logger(name, verbose):
    coloredlogs.install()
    logger = logging.getLogger(name)
    formatter = logging.Formatter(
        fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
    # check if logs direct exists and if not create
    if os.path.exists('./logs'):
        pass
    else:
        logger.info('./logs does not exist...creating')
        os.makedirs('./logs')
        logger.info('created ./logs directory')
    handler = logging.FileHandler(f'./logs/{name}.log')
    logger.addHandler(handler)
    logging.basicConfig(level=logging.INFO)
    handler.setFormatter(formatter)
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    return logger


def main():
    # Setup logging
    logger = setup_custom_logger("mool", args.verbose)
    # Check for a pass mac address
    if not args.mac_address:
        logger.error('You did not pass a mac address...')
        logger.error("Quiting...")
        sys.exit(-1)
    logger.debug(f"MAC address passed is {args.mac_address}")
    # Check the format of the passed mac address and do get if valid
    try:
        mac = EUI(args.mac_address, dialect=mac_cisco)
        logger.debug(str(mac))
        r = requests.get(
            f'https://api.macvendors.com/{str(mac.format())}',
            # f'https://www.macvendorlookup.com/api/v2/{str(mac.format())}',
            verify=False
            )
        if r.status_code == 200:
            # data = json.loads(r.text)
            # logger.info(json.dumps(data, indent=2))
            logger.info(f"{mac} has an OUI assigned to {r.text}")
        elif r.status_code == 204:
            logger.info(f'OUI not found for {str(mac)}')
            logger.debug(f'status code = {r.status_code}')
            logger.debug(f'response = {r.text}')
            sys.exit(-1)
        else:
            logger.error(f'status code = {r.status_code}')
            logger.error(f'response = {r.text}')
            sys.exit(-1)
    except AddrFormatError as format_e:
        logger.error(f"{args.mac_address} is not a valid mac address.")
        raise(format_e)


if __name__ == '__main__':
    requests.packages.urllib3.disable_warnings()

    # Set options for passing arguments to the script
    parser = argparse.ArgumentParser()
    # -f option for specifying the input filename (not required)
    parser.add_argument('-m', '--mac_address', required=True,
                        type=str,
                        default='',
                        help="mac address you'd like to lookup")
    parser.add_argument(
        '-v',
        '--verbose',
        required=False,
        default=False,
        action='store_true',
        help="Set logging to verbose")
    args = parser.parse_args()
    main()
