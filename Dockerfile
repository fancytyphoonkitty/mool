FROM centos:7

COPY . /home/mool
WORKDIR /home/mool

RUN yum check-update; \
    yum install -y gcc libffi-devel python3; \
    yum install -y python3-pip; \
    yum clean all

RUN pip3 install --upgrade pip; \
    pip3 install -r requirements.txt

ENTRYPOINT ["python3", "mool.py"]
